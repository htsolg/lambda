package ua.rv.huts.oleh;

public interface CheckedList {
    boolean checked(int number);
}
