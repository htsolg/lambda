package ua.rv.huts.oleh;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.run();
    }

    private void run() {
        ArrayList<Integer> randomNumbers = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            int num = (int) (Math.random() * 14);
            randomNumbers.add(num);
        }
        System.out.println();
        System.out.println("Random number:");
        System.out.println(randomNumbers);

        System.out.println("sum even: ");
        int sumList = checkedList(randomNumbers, num -> num % 2 == 0);
        System.out.println(sumList);
        System.out.println("sum odd: ");
        int sumList2 = checkedList(randomNumbers, num -> num % 2 == 1);
        System.out.println(sumList2);
    }

    private int checkedList(List<Integer> input, CheckedList checkedList){
        int sum = 0;
        List<Integer> resultList = new ArrayList<>();
        for(int num : input){
            if(checkedList.checked(num)){
                resultList.add(num);
                sum += num;
            }
        }
        return sum;
    }
}